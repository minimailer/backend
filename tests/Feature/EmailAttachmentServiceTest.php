<?php

namespace Tests\Feature;

use App\Models\Email;
use App\Models\EmailAttachment;
use App\Repositories\EmailAttachmentRepository;
use App\Services\EmailAttachmentService;
use App\Services\StorageService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class EmailAttachmentServiceTest extends TestCase
{
    use RefreshDatabase;

    private EmailAttachmentService $emailAttachmentService;
    private Email $email;

    public function setUp(): void
    {
        parent::setUp();
        $this->emailAttachmentService = new EmailAttachmentService(new StorageService(), new EmailAttachmentRepository());
        $this->email = Email::factory()->hasAttachments(5)->create();
    }

    public function testInsert()
    {
        $fakeFile = UploadedFile::fake()->create('FileName', 2);
        $attachment = $this->emailAttachmentService->insert($this->email->getId(), $fakeFile);
        $this->assertEquals($this->email->getId(), $attachment->getEmailId());

        /** @var EmailAttachment[] $attachments */
        $attachments = $this->email->attachments;
        foreach ($attachments as $attachment) {
            $this->assertEquals($this->email->getId(), $attachment->getEmailId());
        }
    }

    public function testDestroy()
    {
        /** @var EmailAttachment $deletedAttachment */
        $deletedAttachment = $this->email->attachments()->get()[0];

        $this->emailAttachmentService->destroy($deletedAttachment->getId());

        /** @var EmailAttachment[] $attachments */
        $attachments = $this->email->attachments()->get();
        foreach ($attachments as $attachment) {
            $this->assertNotEquals($deletedAttachment->getId(), $attachment->getId());
        }
    }
}
