<?php

namespace Tests\Feature;

use App\Entities\Emails\EmailsFilterEntity;
use App\Entities\Emails\InsertEmailEntity;
use App\Models\Email;
use App\Models\EmailAttachment;
use App\Repositories\EmailRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EmailRepositoryTest extends TestCase
{
    use RefreshDatabase;

    private EmailRepository $emailRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->emailRepository = new EmailRepository();
    }

    public function testGetById()
    {
        $email = Email::factory()->create();
        $emailGetById = $this->emailRepository->findById($email->getId());
        $this->assertEquals($email->getId(), $emailGetById->getId());
    }

    public function testList()
    {
        Email::factory()->count(10)->create();
        $emailsFilterEntity = new EmailsFilterEntity();
        $emailList = $this->emailRepository->list($emailsFilterEntity);

        foreach ($emailList->items() as $email) {
            $this->assertNotEquals(Email::DRAFT_STATUS, $email->getStatus());
        }

        /** @var Email $emailFiltered */
        $emailFiltered = $emailList->items()[0];
        $emailsFilterEntity->setFrom($emailFiltered->getFrom());
        $emailList = $this->emailRepository->list($emailsFilterEntity);

        foreach ($emailList->items() as $email) {
            $this->assertEquals($emailFiltered->getFrom(), $email->getFrom());
        }
    }

    public function testEmailGetDraft()
    {
        $draftEmail = $this->emailRepository->getDraft();

        $this->assertInstanceOf(Email::class, $draftEmail);
        $this->assertEquals(Email::DRAFT_STATUS, $draftEmail->getStatus());

        $draftEmailId = $draftEmail->getId();
        $draftEmail = $this->emailRepository->getDraft();

        $this->assertEquals($draftEmailId, $draftEmail->getId());
    }

    public function testSaveDraft()
    {
        Email::factory()->draft()->create();
        $emailEntity = new InsertEmailEntity(
            'from@gmail.com',
            'to@gmail.com',
            'subject',
            'Text',
            'HTML'
        );

        $this->emailRepository->saveDraft($emailEntity);
        $draftEmail = $this->emailRepository->getDraft();
        $this->assertEquals('from@gmail.com', $draftEmail->getFrom());
    }

    public function testSubmitDraft()
    {
        $draftEmail = Email::factory()->draft()->create();
        $this->emailRepository->submitDraft();

        $postedEmail = $this->emailRepository->findById($draftEmail->getId());
        $this->assertEquals(Email::POSTED_STATUS, $postedEmail->getStatus());
    }

    public function testGetAttachments()
    {
        $attachmentsCount = rand(1,5);
        $draftEmail = Email::factory()->draft()->hasAttachments($attachmentsCount)->create();
        $this->assertCount($attachmentsCount, $draftEmail->attachments);
        $this->assertInstanceOf(EmailAttachment::class, $draftEmail->attachments->first());
    }

    public function testSetFailed()
    {
        $email = Email::factory()->create();
        $this->emailRepository->setFailed($email->getId());

        $failedEmail = $this->emailRepository->findById($email->getId());
        $this->assertEquals(Email::FAILED_STATUS, $failedEmail->getStatus());
    }

    public function testSetSent()
    {
        $email = Email::factory()->create();
        $this->emailRepository->setSent($email->getId());

        $sentEmail = $this->emailRepository->findById($email->getId());
        $this->assertEquals(Email::SENT_STATUS, $sentEmail->getStatus());
    }
}
