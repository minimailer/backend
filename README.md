### Project deployment

- composer install
- copy .env.example as .env
- change database connections inside .env file
- set QUEUE_CONNECTION=redis inside .env
- php artisan serve
- php artisan queue:work --tries=5
- start sending requests

### Testing

- change DB_TEST_* properties in .env file for testing database
- run command "php artisan test" or "./vendor/bin/phpunit"
