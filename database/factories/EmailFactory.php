<?php

namespace Database\Factories;

use App\Models\Email;
use App\Models\EmailAttachment;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Email::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            Email::FIELD_FROM => $this->faker->email,
            Email::FIELD_TO => $this->faker->email,
            Email::FIELD_SUBJECT => $this->faker->sentence,
            Email::FIELD_STATUS => $this->faker->randomElement([Email::POSTED_STATUS, Email::FAILED_STATUS, Email::SENT_STATUS]),
            Email::FIELD_TEXT_CONTENT => $this->faker->text,
            Email::FIELD_HTML_CONTENT => $this->faker->text,
        ];
    }

    public function draft(): EmailFactory
    {
        return $this->state(function (array $attributes) {
            return [
                Email::FIELD_STATUS => Email::DRAFT_STATUS
            ];
        });
    }

    public function hasAttachments(int $attachmentsCount): EmailFactory
    {
        return $this->has(EmailAttachment::factory()->count($attachmentsCount), 'attachments');
    }
}
