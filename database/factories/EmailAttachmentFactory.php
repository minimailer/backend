<?php

namespace Database\Factories;

use App\Models\EmailAttachment;
use App\Services\StorageService;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailAttachmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmailAttachment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            EmailAttachment::FIELD_ORIGINAL_NAME => $this->faker->word,
            EmailAttachment::FIELD_NAME => $this->faker->unique()->word,
            EmailAttachment::FIELD_PATH => StorageService::ATTACHMENT_PATH,
            EmailAttachment::FIELD_EXTENSION => $this->faker->fileExtension,
        ];
    }
}
