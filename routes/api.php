<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/attachments', 'EmailAttachmentsController@store')->name('attachments.insert');
Route::delete('/attachments/{id}', 'EmailAttachmentsController@delete')->name('attachments.delete');

Route::get('/emails', 'EmailController@index')->name('email.index');
Route::get('/emails/{emailId}/attachments', 'EmailController@getAttachments')->name('email.attachments');

Route::post('/emails/draft', 'EmailController@submitDraft')->name('email.draft.submit');
Route::get('/emails/draft', 'EmailController@draft')->name('email.draft');
Route::put('/emails/draft', 'EmailController@saveDraft')->name('email.draft.save');
