<?php

namespace App\Mail;

use App\Models\Email;
use App\Models\EmailAttachment;
use App\Services\StorageService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /** @var EmailAttachment[] */
    public array $emailAttachments;
    public Email $email;
    public StorageService $storageService;

    public function __construct(Email $email, array $emailAttachments)
    {
        $this->email = $email;
        $this->emailAttachments = $emailAttachments;
        $this->storageService = new StorageService();
    }

    public function build()
    {
        $email = $this->markdown('email')
            ->from($this->email->getFrom())
            ->subject($this->email->getSubject());

        foreach ($this->emailAttachments as $attachment) {
            if($this->storageService->isFileExist($attachment->getFullname())) {
                $email->attachFromStorage(
                    $attachment->getFullname(),
                    $attachment->getOriginalName().'.'.$attachment->getExtension()
                );
            }
        }

        return $email;
    }
}
