<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadEmailAttachmentRequest;
use App\Http\Resources\InsertAttachmentResource;
use App\Repositories\EmailAttachmentRepository;
use App\Services\EmailAttachmentService;
use Illuminate\Http\JsonResponse;

class EmailAttachmentsController extends Controller
{
    private EmailAttachmentService $attachmentService;

    public function __construct(EmailAttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function store(UploadEmailAttachmentRequest $request): InsertAttachmentResource
    {
        $attachment = $request->file(UploadEmailAttachmentRequest::PARAM_ATTACHMENT);
        $emailId = $request->get(UploadEmailAttachmentRequest::PARAM_EMAIL_ID);
        $attachment = $this->attachmentService->insert($emailId, $attachment);
        return new InsertAttachmentResource($attachment);
    }

    public function delete(int $id): JsonResponse
    {
        $this->attachmentService->destroy($id);
        return response()->json(null);
    }
}
