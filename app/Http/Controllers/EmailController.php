<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailsFetchRequest;
use App\Http\Requests\StoreEmailRequest;
use App\Http\Resources\EmailAttachmentListResource;
use App\Http\Resources\EmailListResource;
use App\Jobs\SendMail;
use App\Repositories\EmailRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EmailController extends Controller
{
    private EmailRepository $emailRepository;

    public function __construct(EmailRepository $emailRepository)
    {
        $this->emailRepository = $emailRepository;
    }

    public function index(EmailsFetchRequest $request): AnonymousResourceCollection
    {
        $filterEntity = $request->extractFilterEntity();
        $list = $this->emailRepository->list($filterEntity);
        return EmailListResource::collection($list);
    }

    public function draft(): EmailListResource
    {
        $draftEmail = $this->emailRepository->getDraft();
        return new EmailListResource($draftEmail);
    }

    public function saveDraft(StoreEmailRequest $request): JsonResponse
    {
        $emailEntity = $request->extractEntity();
        $this->emailRepository->saveDraft($emailEntity);
        return response()->json(null);
    }

    public function submitDraft(StoreEmailRequest $request): JsonResponse
    {
        $emailEntity = $request->extractEntity();
        $this->emailRepository->saveDraft($emailEntity);

        $processedEmail = $this->emailRepository->getDraft();
        $this->emailRepository->submitDraft();

        $attachments = $this->emailRepository->getAttachments($processedEmail->getId());
        SendMail::dispatch($processedEmail, $attachments);

        return response()->json(null);
    }

    public function getAttachments(int $emailId): AnonymousResourceCollection
    {
        $attachments = $this->emailRepository->getAttachments($emailId);
        return EmailAttachmentListResource::collection($attachments);
    }
}
