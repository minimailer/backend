<?php

namespace App\Http\Requests;

use App\Entities\Emails\InsertEmailEntity;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmailRequest extends FormRequest
{
    const PARAM_FROM = 'from';
    const PARAM_TO = 'to';
    const PARAM_SUBJECT = 'subject';
    const PARAM_TEXT_CONTENT = 'textContent';
    const PARAM_HTML_CONTENT = 'htmlContent';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            self::PARAM_FROM => 'required|email:filter|max:255',
            self::PARAM_TO => 'required|email:filter|max:255',
            self::PARAM_SUBJECT => 'required|string|max:255',
            self::PARAM_TEXT_CONTENT => "required_without:htmlContent|max:65535",
            self::PARAM_HTML_CONTENT => "required_without:textContent|max:65535",
        ];
    }

    public function extractEntity(): InsertEmailEntity
    {
        return new InsertEmailEntity(
            $this->{self::PARAM_FROM},
            $this->{self::PARAM_TO},
            $this->{self::PARAM_SUBJECT},
            $this->{self::PARAM_TEXT_CONTENT},
            $this->{self::PARAM_HTML_CONTENT},
        );
    }
}
