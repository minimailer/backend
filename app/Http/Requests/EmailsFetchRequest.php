<?php

namespace App\Http\Requests;

use App\Entities\Emails\EmailsFilterEntity;
use Illuminate\Foundation\Http\FormRequest;

class EmailsFetchRequest extends FormRequest
{
    const PARAM_FILTER = 'filter';
    const PARAM_FILTER_FROM = self::PARAM_FILTER.'.from';
    const PARAM_FILTER_TO = self::PARAM_FILTER.'.to';
    const PARAM_FILTER_SUBJECT = self::PARAM_FILTER.'.subject';
    const PARAM_FILTER_STATUS = self::PARAM_FILTER.'.status';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            self::PARAM_FILTER => 'array|nullable',
            self::PARAM_FILTER_FROM => 'string|nullable',
            self::PARAM_FILTER_TO => 'string|nullable',
            self::PARAM_FILTER_SUBJECT => 'string|nullable',
            self::PARAM_FILTER_STATUS => 'string|nullable',
        ];
    }

    public function extractFilterEntity(): EmailsFilterEntity
    {
        return new EmailsFilterEntity(
            $this->input(self::PARAM_FILTER_FROM),
            $this->input(self::PARAM_FILTER_TO),
            $this->input(self::PARAM_FILTER_SUBJECT),
            $this->input(self::PARAM_FILTER_STATUS),
        );
    }
}
