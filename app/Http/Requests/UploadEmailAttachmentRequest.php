<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadEmailAttachmentRequest extends FormRequest
{
    const PARAM_EMAIL_ID = 'email_id';
    const PARAM_ATTACHMENT = 'attachment';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            self::PARAM_EMAIL_ID => 'required|exists:emails,id',
            self::PARAM_ATTACHMENT => 'required|file'
        ];
    }
}
