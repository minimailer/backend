<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

abstract class QueryFilter
{
    protected FilterEntityInterface $filterEntity;
    protected Builder $builder;

    /**
     * QueryFilter constructor.
     * @param FilterEntityInterface $filterEntity
     */
    public function __construct(FilterEntityInterface $filterEntity)
    {
        $this->filterEntity = $filterEntity;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->filterEntity->extractFilter() as $name => $value) {
            if (method_exists($this, $name)) {
                call_user_func_array([$this, $name], [$value]);
            }
        }

        return $this->builder;
    }
}

