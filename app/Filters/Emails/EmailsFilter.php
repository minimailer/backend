<?php

namespace App\Filters\Emails;

use App\Filters\QueryFilter;
use App\Models\Email;
use Illuminate\Database\Eloquent\Builder;

class EmailsFilter extends QueryFilter
{
    public function to(?string $to): Builder
    {
        return $this->builder->where(Email::FIELD_TO, 'like', "%$to%");
    }

    public function from(?string $from): Builder
    {
        return $this->builder->where(Email::FIELD_FROM, 'like', "%$from%");
    }

    public function subject(?string $subject): Builder
    {
        return $this->builder->where(Email::FIELD_SUBJECT, 'like', "%$subject%");
    }

    public function status(?string $status): Builder
    {
        return $this->builder->where(Email::FIELD_STATUS, 'like', "%$status%");
    }
}
