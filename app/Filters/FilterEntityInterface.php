<?php

namespace App\Filters;

interface FilterEntityInterface
{
    public function extractFilter(): array;
}
