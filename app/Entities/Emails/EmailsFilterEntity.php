<?php

namespace App\Entities\Emails;

use App\Filters\FilterEntityInterface;

class EmailsFilterEntity implements FilterEntityInterface
{
    private ?string $from;
    private ?string $to;
    private ?string $subject;
    private ?string $status;

    public function __construct(?string $from = '',
                                ?string $to = '',
                                ?string $subject = '',
                                ?string $status = '')
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $from
     */
    public function setFrom(?string $from): void
    {
        $this->from = $from;
    }

    /**
     * @param string|null $to
     */
    public function setTo(?string $to): void
    {
        $this->to = $to;
    }

    /**
     * @param string|null $subject
     */
    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    public function extractFilter(): array
    {
        return array_filter([
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
            'subject' => $this->getSubject(),
            'status' => $this->getStatus(),
        ]);
    }
}
