<?php

namespace App\Entities\Emails;

class InsertEmailEntity
{
    private string $from;
    private string $to;
    private string $subject;
    private ?string $textContent;
    private ?string $htmlContent;

    public function __construct(string $from,
                                string $to,
                                string $subject,
                                ?string $textContent = "",
                                ?string $htmlContent = "")
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->textContent = $textContent;
        $this->htmlContent = $htmlContent;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return ?string
     */
    public function getTextContent(): ?string
    {
        return $this->textContent;
    }

    /**
     * @return string
     */
    public function getHtmlContent(): ?string
    {
        return $this->htmlContent;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param ?string $textContent
     */
    public function setTextContent(?string $textContent): void
    {
        $this->textContent = $textContent;
    }

    /**
     * @param ?string $htmlContent
     */
    public function setHtmlContent(?string $htmlContent): void
    {
        $this->htmlContent = $htmlContent;
    }

    public function toDatabase(): array
    {
        return [
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
            'subject' => $this->getSubject(),
            'text_content' => $this->getTextContent(),
            'html_content' => $this->getHtmlContent(),
        ];
    }
}
