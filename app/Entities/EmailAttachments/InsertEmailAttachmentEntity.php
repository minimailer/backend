<?php

namespace App\Entities\EmailAttachments;

class InsertEmailAttachmentEntity
{
    private int    $emailId;
    private string $path;
    private string $name;
    private string $originalName;
    private string $extension;

    public function __construct(int $emailId, string $path, string $originalName, string $name, string $extension)
    {
        $this->emailId = $emailId;
        $this->path = $path;
        $this->originalName = $originalName;
        $this->name = $name;
        $this->extension = $extension;
    }

    /**
     * @return int
     */
    public function getEmailId(): int
    {
        return $this->emailId;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @param string $originalName
     */
    public function setOriginalName(string $originalName): void
    {
        $this->originalName = $originalName;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }

    public function toDatabase(): array
    {
        return [
            'email_id' => $this->emailId,
            'path' => $this->path,
            'original_name' => $this->originalName,
            'name' => $this->name,
            'extension' => $this->extension,
        ];
    }
}
