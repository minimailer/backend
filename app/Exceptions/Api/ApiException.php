<?php

namespace App\Exceptions\Api;

use Exception;

abstract class ApiException extends Exception
{
    public $message;
    public $code;
}
