<?php

namespace App\Exceptions\Api;

class AbcException extends ApiException
{
    public $message = 'Abc exception';
    public $code = 422;
}
