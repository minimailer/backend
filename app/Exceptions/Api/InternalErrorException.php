<?php

namespace App\Exceptions\Api;

class InternalErrorException extends ApiException
{
    public $message = 'Something went wrong';
    public $code = 500;
}
