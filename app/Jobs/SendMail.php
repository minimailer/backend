<?php

namespace App\Jobs;

use App\Models\Email;
use App\Models\EmailAttachment;
use App\Repositories\EmailRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Throwable;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var EmailAttachment[] */
    public array $attachments;
    public Email $email;
    public EmailRepository $emailRepository;

    public function __construct(Email $email, array $attachments)
    {
        $this->email = $email;
        $this->attachments = $attachments;
        $this->emailRepository = new EmailRepository();
    }

    public function handle()
    {
        Mail::to($this->email->getTo())
            ->send(new \App\Mail\SendMail($this->email, $this->attachments));

        $this->emailRepository->setSent($this->email->getId());

        Log::info('Email sent. ID: '.$this->email->getId());
    }

    public function failed(Throwable $exception)
    {
        $this->emailRepository->setFailed($this->email->getId());
        Log::info($exception->getMessage());
    }
}
