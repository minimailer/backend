<?php

namespace App\Repositories;

use App\Entities\Emails\InsertEmailEntity;
use App\Filters\Emails\EmailsFilter;
use App\Filters\FilterEntityInterface;
use App\Models\Email;
use App\Models\EmailAttachment;
use App\Models\EmailAttachment as EmailAttachmentModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EmailRepository
{
    public function findById(int $emailId)
    {
        return Email::find($emailId);
    }

    public function list(FilterEntityInterface $filterEntity, int $limit = 10): LengthAwarePaginator
    {
        $filterInstance = new EmailsFilter($filterEntity);
        return Email::filter($filterInstance)
            ->where(Email::FIELD_STATUS, '!=', Email::DRAFT_STATUS)
            ->with('attachments')
            ->paginate($limit);
    }

    public function getDraft(): Email
    {
        return Email::firstOrCreate([Email::FIELD_STATUS => Email::DRAFT_STATUS]);
    }

    public function saveDraft(InsertEmailEntity $emailEntity): bool
    {
        return Email::where(Email::FIELD_STATUS, Email::DRAFT_STATUS)
            ->update($emailEntity->toDatabase());
    }

    public function submitDraft(): bool
    {
        return Email::where(Email::FIELD_STATUS, Email::DRAFT_STATUS)
            ->update([Email::FIELD_STATUS => Email::POSTED_STATUS]);
    }

    public function getAttachments(int $emailId): array
    {
        return EmailAttachmentModel::where(EmailAttachmentModel::FIELD_EMAIL_ID, $emailId)
            ->get()
            ->all();
    }

    public function setFailed(int $emailId): bool
    {
        return Email::where(Email::FIELD_ID, $emailId)
            ->update([Email::FIELD_STATUS => Email::FAILED_STATUS]);
    }

    public function setSent(int $emailId): bool
    {
        return Email::where(Email::FIELD_ID, $emailId)
            ->update([Email::FIELD_STATUS => Email::SENT_STATUS]);
    }
}
