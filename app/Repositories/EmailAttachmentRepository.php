<?php

namespace App\Repositories;

use App\Entities\EmailAttachments\InsertEmailAttachmentEntity;
use App\Models\EmailAttachment;

class EmailAttachmentRepository
{
    public function findById(int $id): ?EmailAttachment
    {
        return EmailAttachment::find($id);
    }

    public function insert(InsertEmailAttachmentEntity $entity)
    {
        return EmailAttachment::create($entity->toDatabase());
    }

    public function destroyById(int $id): int
    {
        return EmailAttachment::destroy($id);
    }
}
