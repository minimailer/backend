<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StorageService
{
    private string $storage;
    private string $path;

    public const STORAGE_S3 = 's3';
    public const STORAGE_LOCAL = 'local';

    public const ATTACHMENT_PATH = 'attachments';

    /**
     * StorageService constructor.
     *
     * @param string $storage
     * @param string $path
     */
    public function __construct(string $storage = self::STORAGE_LOCAL, string $path = self::ATTACHMENT_PATH)
    {
        $this->storage = $storage;
        $this->path = $path;
    }

    /**
     * Сохранение файла
     *
     * @param UploadedFile $file
     * @param string $name
     * @param string $extension
     * @return string
     */
    public function storeAs(UploadedFile $file, string $name, string $extension): string
    {
        $file->storeAs($this->path, "{$name}.{$extension}", $this->storage);
        return "{$name}.{$extension}";
    }

    /**
     * @param string $name
     * @param string $file
     * @return bool
     */
    public function put(string $name, string $file): bool
    {
        return Storage::disk($this->storage)->put($name, $file);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function delete(string $name): bool
    {
        return Storage::disk($this->storage)->delete($name);
    }

    /**
     * @param string $name
     * @return string
     */
    public function get(string $name): string
    {
        return Storage::disk($this->storage)->url($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function isFileExist(string $name): bool
    {
        return Storage::disk($this->storage)->exists($name);
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
