<?php

namespace App\Services;

use App\Entities\EmailAttachments\InsertEmailAttachmentEntity;
use App\Models\EmailAttachment;
use App\Repositories\EmailAttachmentRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class EmailAttachmentService
{
    private int $numberLetter = 32;
    private StorageService $storageService;
    private EmailAttachmentRepository $attachmentRepository;

    public function __construct(StorageService $storageService, EmailAttachmentRepository $attachmentRepository)
    {
        $this->storageService = $storageService;
        $this->attachmentRepository = $attachmentRepository;
    }

    public function insert(int $emailId, UploadedFile $file): EmailAttachment
    {
        $fileName = Str::random($this->numberLetter);
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $extension = $file->clientExtension();
        $insertAttachmentEntity = new InsertEmailAttachmentEntity(
            $emailId,
            $this->storageService->getPath(),
            $originalFilename,
            $fileName,
            $extension);

        $this->storageService->storeAs($file, $fileName, $extension);
        return $this->attachmentRepository->insert($insertAttachmentEntity);
    }

    public function destroy(int $attachmentId)
    {
        $attachment = $this->attachmentRepository->findById($attachmentId);

        if($attachment && $this->storageService->isFileExist($attachment->getFullname())) {
            $this->storageService->delete($attachment->getFullname());
        }

        $this->attachmentRepository->destroyById($attachmentId);
    }
}
