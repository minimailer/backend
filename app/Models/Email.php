<?php

namespace App\Models;

use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed id
 * @property mixed from
 * @property mixed to
 * @property mixed subject
 * @property mixed text_content
 * @property mixed html_content
 * @property mixed created_at
 * @property mixed updated_at
 * @mixin Builder
 */
class Email extends Model
{
    use HasFactory;

    const FIELD_ID = 'id';
    const FIELD_FROM = 'from';
    const FIELD_TO = 'to';
    const FIELD_SUBJECT = 'subject';
    const FIELD_TEXT_CONTENT = 'text_content';
    const FIELD_HTML_CONTENT = 'html_content';
    const FIELD_STATUS = 'status';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    const DRAFT_STATUS = 'Draft';
    const POSTED_STATUS = 'Posted';
    const SENT_STATUS = 'Sent';
    const FAILED_STATUS = 'Failed';

    protected $fillable = [
        self::FIELD_FROM,
        self::FIELD_TO,
        self::FIELD_SUBJECT,
        self::FIELD_STATUS,
        self::FIELD_TEXT_CONTENT,
        self::FIELD_HTML_CONTENT,
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->{self::FIELD_ID};
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->{self::FIELD_FROM};
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->{self::FIELD_TO};
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->{self::FIELD_SUBJECT};
    }

    /**
     * @return string|null
     */
    public function getTextContent(): ?string
    {
        return $this->{self::FIELD_TEXT_CONTENT};
    }

    /**
     * @return string|null
     */
    public function getHtmlContent(): ?string
    {
        return $this->{self::FIELD_HTML_CONTENT};
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->{self::FIELD_STATUS};
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->{self::FIELD_CREATED_AT};
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->{self::FIELD_UPDATED_AT};
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): void
    {
        $this->{self::FIELD_FROM} = $from;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to): void
    {
        $this->{self::FIELD_TO} = $to;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->{self::FIELD_SUBJECT} = $subject;
    }

    /**
     * @param string|null $textContent
     */
    public function setTextContent(?string $textContent): void
    {
        $this->{self::FIELD_TEXT_CONTENT} = $textContent;
    }

    /**
     * @param string|null $htmlContent
     */
    public function setHtmlContent(?string $htmlContent): void
    {
        $this->{self::FIELD_HTML_CONTENT} = $htmlContent;
    }

    /**
     * @param string status
     */
    public function setStatus(string $status): void
    {
        $this->{self::FIELD_HTML_CONTENT} = $status;
    }

    /**
     * @return HasMany
     */
    public function attachments(): HasMany
    {
        return $this->hasMany(EmailAttachment::class, EmailAttachment::FIELD_EMAIL_ID, Email::FIELD_ID);
    }

    /**
     * @param Builder $query
     * @param QueryFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $query, QueryFilter $filter): Builder
    {
        return $filter->apply($query);
    }
}
