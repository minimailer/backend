<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed id
 * @property mixed email_id
 * @property mixed path
 * @property mixed name
 * @property mixed extension
 * @property mixed created_at
 * @property mixed updated_at
 * @mixin Builder
 */
class EmailAttachment extends Model
{
    use HasFactory;

    const FIELD_ID = 'id';
    const FIELD_EMAIL_ID = 'email_id';
    const FIELD_ORIGINAL_NAME = 'original_name';
    const FIELD_NAME = 'name';
    const FIELD_PATH = 'path';
    const FIELD_EXTENSION = 'extension';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    protected $fillable = [
        self::FIELD_EMAIL_ID,
        self::FIELD_ORIGINAL_NAME,
        self::FIELD_NAME,
        self::FIELD_PATH,
        self::FIELD_EXTENSION,
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->{self::FIELD_ID};
    }

    /**
     * @return int
     */
    public function getEmailId(): int
    {
        return $this->{self::FIELD_EMAIL_ID};
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->{self::FIELD_PATH};
    }

    /**
     * @return string
     */
    public function getOriginalName(): string
    {
        return $this->{self::FIELD_ORIGINAL_NAME};
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->{self::FIELD_NAME};
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->{self::FIELD_EXTENSION};
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->{self::FIELD_CREATED_AT};
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->{self::FIELD_UPDATED_AT};
    }

    /**
     * @return string
     */
    public function getFullname(): string
    {
        return $this->{self::FIELD_PATH}.'/'.$this->{self::FIELD_NAME}.'.'.$this->{self::FIELD_EXTENSION};
    }

    /**
     * @param int $email_id
     */
    public function setEmailId(int $email_id): void
    {
        $this->{self::FIELD_EMAIL_ID} = $email_id;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->{self::FIELD_PATH} = $path;
    }

    /**
     * @param string $originalName
     */
    public function setOriginalName(string $originalName): void
    {
        $this->{self::FIELD_ORIGINAL_NAME} = $originalName;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->{self::FIELD_NAME} = $name;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->{self::FIELD_EXTENSION} = $extension;
    }

    /**
     * @return BelongsTo
     */
    public function email(): BelongsTo
    {
        return $this->belongsTo(Email::class);
    }
}
